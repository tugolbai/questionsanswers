import { Component, OnInit } from '@angular/core';
import { QuestionModel } from './shared/question.model';
import { QuestionService } from './shared/question.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements  OnInit{
  questionItems!: QuestionModel[];

  constructor(private questionService: QuestionService) {}

  ngOnInit(): void {
    this.questionItems = this.questionService.getItems();
    this.questionService.questionItemsChange.subscribe((questionItems: QuestionModel[]) => {
      this.questionItems = questionItems;
    });
  }

  getTotal() {
    let count = 0;
    this.questionItems.forEach(ans => {
      if (ans.status === 'ответ верный') {
        count++;
      }
    })
    return count;
  }

}
