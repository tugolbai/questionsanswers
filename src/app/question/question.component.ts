import { Component, Input } from '@angular/core';
import { QuestionModel } from '../shared/question.model';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent  {
  @Input() question!: QuestionModel;
  onPrompt = false;

  onClick() {
    this.onPrompt = !this.onPrompt;
  }


}
