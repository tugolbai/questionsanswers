import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AnswerComponent } from './answer/answer.component';
import { QuestionService } from './shared/question.service';
import { QuestionComponent } from './question/question.component';
import { BackgroundColorDirective } from './directives/backgroundColor.directive';
import { ShowPromptDirective } from './directives/showPrompt.directive';

@NgModule({
  declarations: [
    AppComponent,
    AnswerComponent,
    QuestionComponent,
    BackgroundColorDirective,
    ShowPromptDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
