import { EventEmitter } from '@angular/core';
import { QuestionModel } from './question.model';

export class QuestionService {
  questionItemsChange = new EventEmitter<QuestionModel[]>();

  private questionItems: QuestionModel[] = [
    new QuestionModel('Что не вместится даже в самую болшую кастрюю?', 'Крышка', 'Чем закрывают' +
      ' кастрюлю?)'),
    new QuestionModel('Каких камней не бывает в речке?', 'Сухих', 'Сухих'),
    new QuestionModel('Кто в году четыре раза переодевается?', 'Земля', 'Земля'),
    new QuestionModel('У машины будет много сил,\n' +
      'Если выпьет свой она...', 'Бензин', 'Бензин'),
    new QuestionModel('У квадратного стола отпилили один угол. Сколько теперь углов у него стало?',
      'Пять', 'Пять'),
  ];

  getItems() {
    return this.questionItems.slice();
  }

}
