export class QuestionModel {
  constructor(
    public question: string,
    public answer: string,
    public prompt: string,
    public status: string = 'нет ответа',
  ) {}
}
