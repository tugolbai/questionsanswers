import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent  {
  @ViewChild('answerInput') answerInput!: ElementRef;


  setAnswer() {
    console.log(this.answerInput.nativeElement.value);
  }
}
