import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBackgroundColor]'
})

export class BackgroundColorDirective  {
  @Input() set appBackgroundColor(bgClass: string) {
    if (bgClass === 'ответ верный') {
      this.bgClass = 'bg-success';
    } else if (bgClass === 'ответ неверный') {
      this.bgClass = 'bg-danger';
    }
  }
  bgClass = 'bg-light';

  constructor(private el: ElementRef, private renderer: Renderer2) {
      this.renderer.addClass(this.el.nativeElement, this.bgClass);
  }
}
